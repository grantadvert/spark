import org.slf4j.LoggerFactory
import infrastructure._

object Main extends App {
  lazy val logger = LoggerFactory.getLogger("Main")

  override def main(args: Array[String]): Unit = {
    SparkUtils.withSpark { sc =>
      val res = sc.makeRDD(Array[Int](1,2,3,4,5)).reduce(_+_)
      println(res)
    
    }
  }
}
