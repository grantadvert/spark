package infrastructure

import java.io.File

import com.typesafe.config.{ConfigFactory, Config}

object ConfigUtils {
  def config() : Config = {
    val fileName = "../conf/spark.conf"
    val mainConfig = if (FileUtils.exist(fileName)) {
      ConfigFactory.parseFile(new File(fileName))
    }
    else {
      ConfigFactory.empty()
    }

    mainConfig.withFallback(ConfigFactory.load)
  }
}
