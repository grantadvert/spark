package infrastructure

import com.github.nscala_time.time.RichReadableInstant
import org.apache.spark.rdd.RDD
import org.apache.spark._
import org.joda.time.DateTime


/**
 * Spark utils
 */
object SparkUtils {
  lazy val serverName: String = {
    val fileName = "/etc/hostname"
    if (FileUtils.exist(fileName)) {
      FileUtils.readFile(fileName)
    } else {
      "unknownServer"
    }
  }

  private def sparkConf(master: String, appName: String) = {
    new SparkConf()
      .setMaster(master)
      .setAppName(appName)
      .set("spark.local.dir", ConfigUtils.config().getString("spark.tmpdir"))
      .set("spark.executor.memory", ConfigUtils.config().getString("spark.executor.memory"))
      .set("spark.driver.maxResultSize", ConfigUtils.config().getString("spark.driver.maxResultSize"))
      .set("spark.eventLog.enabled", ConfigUtils.config().getString("spark.eventLog.enabled"))
      .set("spark.eventLog.dir", ConfigUtils.config().getString("spark.eventLog.dir"))
  }

  private def spark(appName: String): SparkContext = {
    val conf = sparkConf(ConfigUtils.config().getString("spark.master.main"), appName)
    val sc = new SparkContext(conf)

    sc
  }


  def withSpark[A](func: SparkContext => A): A = {
    withSpark()(func)
  }

  def withSpark[A](appName: String = "test")(func: SparkContext => A): A = {
    val sc = spark(s"${serverName}_${appName}")
    try {
      func(sc)
    } finally {
      sc.stop()
    }
  }

}

