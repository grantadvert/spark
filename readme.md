��������� spark (library version).

�� ������ ���������� ���������:
-- JDK 7.0/8.0 (���������� oracle, � �� openJDK)
-- sbt (simple build tool) http://www.scala-sbt.org


������������ ������. 
git clone https://grantadvert@bitbucket.org/grantadvert/spark.git

����� � ������: 
> cd spark

��������� sbt: 
> sbt

������ �������:
sbt > run

����������� /src/main/scala/Main.scala. ����������� ������ ������.
��� ��������� ����� ������������ �����������
sc.textFile("fileName") (��� ��������� RDD[String])

� ������ ��������� ���������� ��� �����������, ����������������.
� /src/main/resources/application.conf ���� ��������� ���������. 
��������, spark.master.main ��������� �� ��������� ������� spark (local[1]). ��� ������� �������� ����� �������� �� "spark://master-node:7077".

��� ��������� ������� � intelij IDEA ������� � sbt "gen-idea". ����� ����� ������ ������� � IDEA. 


������ ��� windows �� ��������, ��� ��� ����������� ����������� �����������. ��� linux/macOS ��������.