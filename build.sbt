name := "spark-ml"

scalaVersion := "2.10.4"

version := "1.0"

libraryDependencies ++= Seq(
    "org.apache.spark" %% "spark-core" % "1.4.0").map({ dep =>
      dep.exclude("org.mortbay.jetty", "servlet-api").
        exclude("commons-beanutils", "commons-beanutils-core").
        exclude("commons-collections", "commons-collections").
        exclude("commons-collections", "commons-collections").
        exclude("com.esotericsoftware.minlog", "minlog").
        exclude("asm", "asm").
        exclude("org.apache.hadoop", "hadoop-yarn-common").
        exclude("org.apache.hadoop", "hadoop-client").
        exclude("javax.servlet", "servlet-api")
    })



libraryDependencies += "joda-time" % "joda-time" % "2.1"

libraryDependencies += "junit" % "junit" % "4.8" % "test"

libraryDependencies += "com.jcraft" % "jsch" % "0.1.51"

libraryDependencies += "com.typesafe" % "config" % "1.2.1"

libraryDependencies += "net.jpountz.lz4" % "lz4" % "1.2.0"

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.5"

libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "1.2.0"

libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.5.0" exclude("javax.servlet", "servlet-api") //exclude("com.google.guava", "guava")

libraryDependencies += "org.apache.hadoop" % "hadoop-hdfs" % "2.5.0" exclude("javax.servlet", "servlet-api") //exclude("com.google.guava", "guava")

libraryDependencies += "org.reflections" % "reflections" % "0.9.9-RC1"

net.virtualvoid.sbt.graph.Plugin.graphSettings

parallelExecution in Test := false

instrumentSettings

parallelExecution in ScoverageTest := false

resolvers += "Typesafe Simple Repository" at
  "http://repo.typesafe.com/typesafe/simple/maven-releases/"

resolvers += "Typesafe Repo" at
  "http://repo.typesafe.com/typesafe/releases/"



libraryDependencies += "org.apache.spark" % "spark-mllib_2.10" % "1.4.0"

libraryDependencies += "com.github.scopt" %% "scopt" % "3.3.0"

resolvers += Resolver.sonatypeRepo("public")
